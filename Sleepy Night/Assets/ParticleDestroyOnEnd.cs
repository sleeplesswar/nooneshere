﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroyOnEnd : MonoBehaviour
{
    private ParticleSystem part;
    void Start()
    {
        part = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (part.isStopped)
        {
            GameObject.Destroy(gameObject);
        }
    }
}

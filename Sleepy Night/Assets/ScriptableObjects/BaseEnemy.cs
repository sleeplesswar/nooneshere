﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Enemy")]
public class NewBehaviourScript : ScriptableObject
{
    public GameObject BaseEnemy;

    public NewBehaviourScript bulletPattern;

    public int movementSpeed;
    public NewBehaviourScript movementPattern;
}

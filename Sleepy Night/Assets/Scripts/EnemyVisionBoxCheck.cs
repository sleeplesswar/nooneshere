﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVisionBoxCheck : MonoBehaviour
{
    //Check Visionbox to fire Bullet through EnemyShoot script.

    public GameObject Viewer;
    ViewerManager viewerScript;
    EnemyShoot Shooter;
    bool inView;
    // Start is called before the first frame update
    void Start()
    {
        //Set component References
        viewerScript = Viewer.GetComponent<ViewerManager>();
        Shooter = gameObject.GetComponent<EnemyShoot>();
    }

    // Update is called once per frame
    void Update()
    {
        if (viewerScript.isPlayerinView())
        {
            Shooter.FireEnemyBullet();
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/Weapon")]
public class WeaponData : ScriptableObject
{
    public int _numOfBullets;
    public int _rateOfFire;
    public bool _automatic;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewerManager : MonoBehaviour
{
    GameObject[] InView;

    bool seen;
    // Start is called before the first frame update
    void Start()
    {
        seen = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            seen = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            seen = false;
        }
    }

    public bool isPlayerinView()
    {
        return seen;
    }
}

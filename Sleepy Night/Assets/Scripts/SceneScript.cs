﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class SceneScript : MonoBehaviour
{
    //This class is a singleton
    public static SceneScript Instance;

    public float endTimer = 4;

    public int sceneToBeLoaded;

    public int numberOfHits;
    public int numberOfShots;
    public int dashCount;
    public int finalHealth;

    public float gameTimer;

    bool sentAnalytics = false;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Update()
    {
        gameTimer += Time.deltaTime;
    }


    // Analytics need to be hooked up in order for the game to run. The last line of code regarding to coroutine needs to be left in otherwise the counter will not run.
    public void EndGame()
    {
        //    if (!sentAnalytics)
        //    {
        //        sentAnalytics = true;

        //        //Sends the number of times the player fired and hit to get the accuracy
        //        AnalyticsEvent.Custom("NumberFired", new Dictionary<string, object>
        //        {
        //            { "Shots fired", numberOfShots }
        //        });

        //        AnalyticsEvent.Custom("NumberHit", new Dictionary<string, object>
        //        {
        //            { "Shots landed", numberOfHits }
        //        });

        //        //Set the dash count to the number of times they dashed this game
        //        AnalyticsEvent.Custom("Dash_Count", new Dictionary<string, object>
        //        {
        //            { "Dash Count", dashCount }
        //        });

        //        //Returns how long the level ran
        //        AnalyticsEvent.Custom("Level_Time", new Dictionary<string, object>
        //        {
        //            { "Level Time", gameTimer }
        //        });

        //        //Returns the final health of the level (And how many times people have died)
        //        AnalyticsEvent.Custom("Final_Health", new Dictionary<string, object>
        //        {
        //            { "Final Health", finalHealth }
        //        });
        //    }

        StartCoroutine(EndGameCountDown());
    }

    IEnumerator EndGameCountDown()  //This method will wait endTimer seconds before loading the specified scene
    {
        yield return new WaitForSeconds(endTimer);
        SceneManager.LoadScene(sceneToBeLoaded);
    }

}

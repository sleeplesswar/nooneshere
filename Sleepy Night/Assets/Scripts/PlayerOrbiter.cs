﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOrbiter : MonoBehaviour
{
    public float rotateSpeed;
    [SerializeField]
    private GameObject followTarget;

    private void Awake()
    {
        //find and follow any players
        if (GameObject.FindGameObjectWithTag("Player") == true)
        {
            followTarget = GameObject.FindGameObjectWithTag("Player");
        }
        //destroy self and children if there are no players
        else
        {
            foreach(Transform child in transform)
            {
                Destroy(child.gameObject);
            }
            Destroy(gameObject);
        }

    }

    void FixedUpdate()
    {
        //constantly spin and follow players
        transform.Rotate(0, rotateSpeed * Time.deltaTime, 0);
        transform.position = followTarget.transform.position;
    }
}

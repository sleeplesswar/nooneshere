﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;

public class Enemy_PlayerPursuit : MonoBehaviour
{
    GameObject PlayerObj;
    UnityEngine.AI.NavMeshAgent agent;

    // Start is called before the first frame update
    void Awake()
    {
        PlayerObj = GameObject.FindWithTag("Player");
        agent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(PlayerObj.transform.position);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{

    [SerializeField]
    AudioClip gunshotSFX;

    AudioSource audioSource;

    public GameObject bullet;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        //If you click shoot
        if (Input.GetMouseButtonDown(0))
        {
            // Grabs the audio source on the object and plays the desired sound effect in the inspector.
            audioSource.PlayOneShot(gunshotSFX);
            //Increments how many bullets have been fired
            SceneScript.Instance.numberOfShots++;
            Instantiate(bullet, transform.position, transform.rotation);
        }
    }



}

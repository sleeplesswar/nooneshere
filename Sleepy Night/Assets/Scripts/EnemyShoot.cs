﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    public GameObject Bullet;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            FireEnemyBullet();
        }
    }


    public void FireEnemyBullet()
    {
        GameObject newBullet = Instantiate(Bullet, gameObject.transform, false);
        newBullet.tag = "EnemyBullet";
    }
}

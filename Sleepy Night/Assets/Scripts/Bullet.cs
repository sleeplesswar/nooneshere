﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _travelSpeed = 10;

    public UnityEvent _bulletHit;
    private Rigidbody _rigidbody;

    public GameObject bulletHitParticle;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        StartCoroutine("DestroyAfterSecondsTimer");
    }

    // Start is called before the first frame update
    private void Update()
    {
        Launch();
    }

    private void Launch()
    {
        _rigidbody.position += transform.right * _travelSpeed * Time.fixedDeltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //If the bullet it hitting the enemy damages
        if(collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<HealthScript>().DamageTarget(1);
            
            //Adds to the number of successful hits
            SceneScript.Instance.numberOfHits++;
            Instantiate(bulletHitParticle, transform.position, transform.rotation);
            _bulletHit.Invoke();
            Destroy(gameObject);
        }
        //If the collision is not hitting the player spawns a bullet hit particle and destroys bullet
        else if (collision.gameObject.tag != "Player")
        {
            _bulletHit.Invoke();
            Destroy(gameObject);
        }

    }

    IEnumerator DestroyAfterSecondsTimer()
    {
        yield return new WaitForSecondsRealtime(10);
        DestroyAfterSeconds();
    }

    void DestroyAfterSeconds()
    {
        StopCoroutine("DestroyAfterSecondsTimer");
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float verticalInput;
    public float horizontalInput;

    public Rigidbody rb;

    public float speed = 5f;

    public float dashCooldown = 3;
    public float dashCooldownReset = 3;

    public Vector3 dashDirection;
    public float dashForce = 100;
    public float dashDuration = 0.3f;
    public float dashDurationTimeLength = 0.3f;

    Ray clickPoint;
    RaycastHit hit;

    private void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        verticalInput = Input.GetAxis("Vertical");
        horizontalInput = Input.GetAxis("Horizontal");

        //If not dashing
        if(dashDuration <= 0)
        {
            rb.velocity = new Vector3(horizontalInput * speed, 0, verticalInput * speed * 2);
        }

        //If the dash is ready to go again 
        if (Input.GetKey(KeyCode.Space) && dashCooldown <= 0 && (Mathf.Abs(verticalInput) > 0 || Mathf.Abs(horizontalInput) > 0))
        {
            dashDirection = new Vector3(horizontalInput, 0, verticalInput).normalized;
            rb.AddForce(new Vector3(dashDirection.x, 0, dashDirection.z * 2f) * dashForce);
            dashCooldown = dashCooldownReset;
            dashDuration = dashDurationTimeLength;

            SceneScript.Instance.dashCount++;
        }

        //Currently dashing
        if(dashDuration > 0)
        {
            dashDuration -= Time.deltaTime;
        }

        //Dashcooldown still counting down
        if(dashCooldown > 0)
        {
            dashCooldown -= Time.deltaTime;
        }

        //get ray through screen
        clickPoint = Camera.main.ScreenPointToRay(Input.mousePosition);

        //turn into raycast hit
        Physics.Raycast(clickPoint, out hit);

        //If the player isn't mousing over the character or near the character
        if (Vector3.Distance(transform.position, hit.point) > 1.5f)
        {
            //Look at that point
            transform.LookAt(new Vector3(hit.point.x, transform.position.y, hit.point.z), Vector3.up);
        }
        

    }

}

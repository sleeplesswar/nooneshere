﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthText : MonoBehaviour
{
    Text healthText;

    public HealthScript healthScript;

    // Start is called before the first frame update
    void Start()
    {
        healthText = GetComponent<Text>();
        healthScript = healthScript.gameObject.GetComponent<HealthScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateHealth()
    {
        //Health text changes based on current value
        if (healthScript.targetHealth > healthScript.maxHealth - (healthScript.maxHealth / 10))
        {
            healthText.color = Color.green;
        }
        else if (healthScript.targetHealth > healthScript.maxHealth - (healthScript.maxHealth / 1.5))
        {
            healthText.color = Color.yellow;
        }
        else
        {
            healthText.color = Color.red;
        }

        //Adjust health text
        healthText.text = $"Health: {healthScript.targetHealth}";
    }
}

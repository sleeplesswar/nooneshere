﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
    public int targetHealth;
    public List<GameObject> alsoDestroy;
    public int maxHealth;
    public GameObject youDiedText;

    public SceneScript ss;

    void Start()
    {
        targetHealth = maxHealth;
        FindObjectOfType<HealthText>().UpdateHealth();
    }

    private void Update()
    {
        //Updates the final health in the scene script
        SceneScript.Instance.finalHealth = targetHealth;

        if (targetHealth <= 0 && this.name == "Player")
        {
            ss.EndGame();
            //This loop is for destroying any objects dependant on the player object
            for (int i = 0; i < alsoDestroy.Count; i++) 
            {
                Destroy(alsoDestroy[i]);   
            }

            //The youDiedText is to inform the player they have just died
            Instantiate(youDiedText, FindObjectOfType<Canvas>().transform); 

            Destroy(this.gameObject);
        }
        else if(targetHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }


    // Call this method on the target to damage it
    public void DamageTarget(int damage)
    {
        targetHealth -= damage;
        FindObjectOfType<HealthText>().UpdateHealth();
    }
}

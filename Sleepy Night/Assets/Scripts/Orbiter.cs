﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbiter : MonoBehaviour
{
    public GameObject hitParticle;

    [SerializeField]
    private bool isRecharging = false;
    public float rechargeTimerMax;

    private float timer;
    private float timerSeconds;

    private void OnTriggerEnter(Collider other)
    {
        //damage any enemies that triggers the orbiter
        if (other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<HealthScript>().DamageTarget(1);

            Instantiate(hitParticle, transform.position, transform.rotation);
            SetRecharge();
        }
        else
        //Reset with an offset when the oribiter is triggered by bullets or the environment
        {
            Instantiate(hitParticle, transform.position, transform.rotation);
            timerSeconds = rechargeTimerMax / 2;
            SetRecharge();
        }
    }

    private void Update()
    {
        //timer for orbiter recharging
        if (isRecharging)
        {
            timer++;
            if (timer % 60 == 0)
            {
                timerSeconds++;
            }

            if (timerSeconds >= rechargeTimerMax)
            {
                gameObject.GetComponent<Collider>().enabled = true;
                gameObject.transform.localScale = (new Vector3(0.5f, 0.5f, 0.5f));
                isRecharging = false;
                timerSeconds = 0;
            }
        }
    }

    void SetRecharge()
    {
        //set orbiter to recharge after being triggered
        timer = 0f;
        gameObject.GetComponent<Collider>().enabled = false;
        gameObject.transform.localScale = (new Vector3(0.25f, 0.25f, 0.25f));
        isRecharging = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class Spawner : MonoBehaviour
{

    public List<Transform> SpawnPoints;
    public GameObject player;

    public int initialWaveCount = 2;
    private int currentWaveCount;
    public int waveIncreaseRate = 1;

    public List<GameObject> currentWave;

    private float waveTimer;
    public float waveTimerStartValue;

    public GameObject enemeyPrefab;

    bool finishedWave;

    private Transform farthestPoint;

    public int wavesToWin;
    private int waveCounter;

    public SceneScript ss;

    public GameObject winText;
    public HealthScript playerHealth;

    private void Start()
    {
        currentWaveCount = initialWaveCount;
        waveTimer = 0;
        waveCounter = 0;
    }

    private void Update()
    {
        //If it's time for the next spawn
        if(waveTimer < 0)
        {
            //Default it IS done wave
            finishedWave = true;

            //Check to see if they are all dead
            for (int i = 0; i < currentWave.Count; i++)
            {
                //If one is not dead
                if(currentWave[i] != null)
                {
                    finishedWave = false;
                }
            }

            //If it has waves to go before winning
            if (finishedWave && waveCounter <= wavesToWin)
            {
                waveCounter++;

                //Find out which spawn point is farthest from player
                for (int i = 0; i < SpawnPoints.Count; i++)
                {
                    if(farthestPoint == null)
                    {
                        farthestPoint = SpawnPoints[i];
                    }
                    //If the distance from old point to player is less than the new points
                    else if (Vector3.Distance(farthestPoint.position, player.transform.position) < Vector3.Distance(SpawnPoints[i].position, player.transform.position))
                    {
                        farthestPoint = SpawnPoints[i];
                    }
                }

                //Clears old wave
                currentWave.Clear();

                //Spawns the next wave
                for (int i = 0; i < currentWaveCount; i++)
                {
                   currentWave.Add(Instantiate(enemeyPrefab, farthestPoint)); 
                }

                //Increments the wave count
                currentWaveCount += waveIncreaseRate;

                //Resets the timer
                waveTimer = waveTimerStartValue;
            }
            //Otherwise they win
            else if(waveCounter > wavesToWin)
            {
                Instantiate(winText, FindObjectOfType<Canvas>().transform);
                ss.EndGame();
            }
        }
        else
        {
            waveTimer -= Time.deltaTime;
        }
    }

}
